import { GET_PRODUCT_WISHLIST, GET_PRODUCT_SELLER, GET_PRODUCT_SOLD } from "../actions/history";

const initialState = {
  getProductSellerResult: false,
  getProductSellerLoading: false,
  getProductSellerError: false,

  getProductWishlistResult: false,
  getProductWishlistLoading: false,
  getProductWishlistError: false,

  getProductSoldResult: false,
  getProductSoldLoading: false,
  getProductSoldError: false,
};

const historyReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_SELLER:
      return {
        ...state,
        getProductSellerResult: action.payload.data,
        getProductSellerLoading: action.payload.loading,
        getProductSellerError: action.payload.errorMessage,
      };
    case GET_PRODUCT_WISHLIST:
      return {
        ...state,
        getProductWishlistResult: action.payload.data,
        getProductWishlistLoading: action.payload.loading,
        getProductWishlistError: action.payload.errorMessage,
      };
    case GET_PRODUCT_SOLD:
      return {
        ...state,
        getProductSoldResult: action.payload.data,
        getProductSoldLoading: action.payload.loading,
        getProductSoldError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export default historyReducer;
