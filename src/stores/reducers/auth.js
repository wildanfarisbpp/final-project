import { GET_LOGIN, GET_REGISTER } from "../actions/auth";

const initialState = {
  getLoginResult: false, 
  getLoginLoading: false, 
  getLoginError: false, 
  getLoginErrorMessage: false, 
  getLoginSuccess: false,

  getRegistResult: false, 
  getRegistLoading: false, 
  getRegistError: false, 
  getRegistErrorMessage: false, 
  getRegistSuccess: false,
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_LOGIN:
          localStorage.setItem("userLocal", JSON.stringify(action.payload.data));
          return {...state,
            getLoginResult: action.payload.data,
            getLoginLoading: action.payload.loading,
            getLoginError: action.payload.error,
            getLoginErrorMessage: action.payload.errorMessage,
            getLoginSuccess: action.payload.success,
          };
        case GET_REGISTER:
          localStorage.setItem("userLocal", JSON.stringify(action.payload));
          return {...state,
            getRegistResult: action.payload.data,
            getRegistLoading: action.payload.loading,
            getRegistError: action.payload.error,
            getRegistErrorMessage: action.payload.errorMessage,
            getRegistSuccess: action.payload.success,
          };
        default:
          return state;
  }
};


export default authReducer;
