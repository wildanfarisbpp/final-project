import { UPDATE_USER_PROFILE, GET_USER_PROFILE } from "../actions/profile";

const initialState = {
    getUpdateProfileResult: false,  getUpdateProfileLoading: false, getUpdateProfileError: false,
    getDetailProfileResult: false,  getDetailProfileLoading: false, getDetailProfileError: false
};

const profileReducer = (state = initialState, action) => {
    switch(action.type){
        case UPDATE_USER_PROFILE:
            return { ...state,
                getUpdateProfileResult: action.payload.data,
                getUpdateProfileLoading: action.payload.loading,
                getUpdateProfileError: action.payload.errorMessage
            };
        case GET_USER_PROFILE:
            return { ...state,
                getDetailProfileResult: action.payload.data,
                getDetailProfileLoading: action.payload.loading,
                getDetailProfileError: action.payload.errorMessage
            };
        default: return state;
    }
}

export default profileReducer