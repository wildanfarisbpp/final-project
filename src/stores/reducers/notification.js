import { GET_NOTIFICATION } from "../actions/notification";
import { EDIT_NOTIFICATION } from "../actions/notification";

const initialState = {
  getNotificationResult: false,
  getNotificationLoading: false,
  getNotificationError: false,

  getEditNotificationResult: false,
  getEditNotificationLoading: false,
  getEditNotificationError: false,
};

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTIFICATION:
      return {
        ...state,
        getNotificationResult: action.payload.data,
        getNotificationLoading: action.payload.loading,
        getNotificationError: action.payload.errorMessage,
      };
    case EDIT_NOTIFICATION:
      return {
        ...state,
        getEditNotificationResult: action.payload.data,
        getEditNotificationLoading: action.payload.loading,
        getEditNotificationError: action.payload.errorMessage,
      };

    default:
      return state;
  }
};

export default notificationReducer;
