import axios from "axios";

export const GET_NOTIFICATION = "GET_NOTIFICATION";
export const EDIT_NOTIFICATION = "EDIT_NOTIFICATION";

export const getNotification = ({ token }) => {
  return async (dispatch) => {
    try {
      //  LOADING
      dispatch({
        type: GET_NOTIFICATION,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      // GET DATA PRODUCT WISHLIST
      const resGetNotification = await axios.get(`https://secondhand-restapi.herokuapp.com/api/notification/get`, { headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response", resGetNotification);
      dispatch({
        type: GET_NOTIFICATION,
        payload: {
          loading: false,
          data: resGetNotification.data,
          errorMessage: false,
        },
      });
      //   CATCH ERROR WHILE FETCH API
    } catch (error) {
      dispatch({
        type: GET_NOTIFICATION,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};

export const editNotification = ({ id, token }) => {
  return async (dispatch) => {
    try {
      //  LOADING
      dispatch({
        type: EDIT_NOTIFICATION,
        payload: {
          loading: true,
          data: false,
          errorMessage: false,
        },
      });
      // GET DATA PRODUCT WISHLIST
      const resEditNotification = await axios({ method: "PUT", url: `https://secondhand-restapi.herokuapp.com/api/notification/read/{id}?id=${id}`, headers: { accept: "*/*", Authorization: `Bearer ${token}` } });
      console.log("Response", resEditNotification);
      dispatch({
        type: EDIT_NOTIFICATION,
        payload: {
          loading: false,
          data: resEditNotification.data,
          errorMessage: false,
        },
      });
      //   CATCH ERROR WHILE FETCH API
    } catch (error) {
      dispatch({
        type: EDIT_NOTIFICATION,
        payload: {
          loading: false,
          data: false,
          errorMessage: error.message,
        },
      });
    }
  };
};
