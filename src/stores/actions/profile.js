import axios from 'axios';

export const UPDATE_USER_PROFILE = "UPDATE_USER_PROFILE"
export const GET_USER_PROFILE = "GET_USER_PROFILE"

export const updateUserProfile = ({values, token}) => {
    return async(dispatch) => {
        try {
            // loading
            dispatch({
                type: UPDATE_USER_PROFILE,
                payload: {
                    loading: true,
                    data: false,
                    errorMessage: false
                }
            })
            console.log('Request', values)
            let formData = new FormData();
            let isiBodyData = { 
                "fullname": values.fullname,
                "city": values.city,
                "address": values.address,
                "phone": values.phone
            }
            formData.append('updateJson', JSON.stringify(isiBodyData))
            formData.append('imageProfil', values.imageProfil)
            const resUpdateProfile = await axios.put(`https://secondhand-restapi.herokuapp.com/api/user/update`, formData, { 
                headers: {
                    "accept": 'application/json',
                    "Content-Type": 'multipart/form-data',
                    "Authorization" : `Bearer ${token}`
                } })
            console.log('Response', resUpdateProfile)
            dispatch({
                type: UPDATE_USER_PROFILE,
                payload: {
                    loading: false,
                    data: resUpdateProfile.data,
                    errorMessage: false
                }
            })
        } catch (error){
            dispatch({
                type: UPDATE_USER_PROFILE,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error.message
                }
            })
        }
    }
}

export const getUserProfile = (token) => {
    return async(dispatch) => {
        try {
            // loading
            dispatch({
                type: GET_USER_PROFILE,
                payload: {
                    loading: true,
                    data: false,
                    errorMessage: false
                }
            })
            const resGetProfile = await axios.get(`https://secondhand-restapi.herokuapp.com/api/user/current-detail`, { headers: { "Authorization" : `Bearer ${token}` } })
            console.log('Response', resGetProfile)
            dispatch({
                type: GET_USER_PROFILE,
                payload: {
                    loading: false,
                    data: resGetProfile.data,
                    errorMessage: false
                }
            })
        } catch (error){
            dispatch({
                type: GET_USER_PROFILE,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error.message
                }
            })
        }
    }
}