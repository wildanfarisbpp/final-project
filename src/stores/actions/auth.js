import axios from "axios";

export const GET_LOGIN = "GET_LOGIN"
export const GET_REGISTER = "GET_REGISTER"

export const getLogin = input => {
    return async(dispatch) => {  
        try {
            dispatch({
                type: GET_LOGIN,
                payload: {
                    loading: true,
                    data: false,
                    success: false,
                    error: false,
                    errorMessage: false
                }
            })
            const resLogin = await axios.post('https://secondhand-restapi.herokuapp.com/api/auth/signin/email', input)
            console.log(resLogin.data); 
            dispatch({
                type: GET_LOGIN,
                payload: {
                    loading: false,
                    data: resLogin.data,
                    success: true,
                    error: false,
                    errorMessage: false
                }
            })
            window.location.assign("/")
        } catch (error){
            console.log(error)
            dispatch({
                type: GET_LOGIN,
                payload: {
                    loading: false,
                    data: false,
                    success: false,
                    error: true,
                    errorMessage: error.response.data.error 
                }
            })
        }
    }
}

export const getRegister = (input) => {
  return async (dispatch) => {
    try {
        dispatch({
            type: GET_REGISTER,
            payload: {
                loading: true,
                data: false,
                success: false,
                error: false,
                errorMessage: false
            }
        })
        const resRegist = await axios.post("https://secondhand-restapi.herokuapp.com/api/auth/signup", input)
        console.log(resRegist)
        dispatch({
            type: GET_REGISTER,
            payload: {
                loading: false,
                data: resRegist.data,
                success: true,
                error: false,
                errorMessage: false
            }
        })
        window.location.assign("/login");
    }catch (error) {
        console.log(error);
        dispatch({
            type: GET_REGISTER,
            payload: {
                loading: false,
                data: false,
                success: false,
                error: true,
                errorMessage: error.response.data.error
            }
        })
      
    }
  };
};
