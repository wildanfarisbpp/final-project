import { Spin } from "antd"
import { LoadingOutlined } from '@ant-design/icons';

function Loading(){
    return (
        <div className="fixed top-1/2 right-1/2 z-50">
            <Spin indicator={<LoadingOutlined />} size="large" spinning="true"></Spin>
        </div>
    )
}
export default Loading