import React from "react";
import { Card } from "antd";
import { Link, useLocation } from "react-router-dom";

function CardProduct({ product }) {
  const location = useLocation();
  return product?.map((item, i) => {
    return (
      <div key={i}>
        {location.pathname === "/" ? (
          <Link to={`/produk-buyer/${item.productId}`}>
            <Card cover={<img src={item.image1 || "/images/default.jpg"} alt="produk" className="px-2 pt-2 h-[100px] object-cover" />} style={{ width: "auto" }} bodyStyle={{ padding: "8px" }} className="drop-shadow">
              <p className="text-body-14 mb-1 truncate">{item.productName}</p>
              <p className="text-body-10 text-neutral03 mb-1">{item.category}</p>
              <p className="text-body-14 mb-1">
                Rp{" "}
                {parseInt(item.price)
                  .toFixed(2)
                  .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                  .toString()}
              </p>
            </Card>
          </Link>
        ) : (
          <Link to={`/produk-seller/${item.productId}`}>
            <Card cover={<img src={item.image1 || "/images/default.jpg"} alt="produk" className="px-2 pt-2 h-[100px] object-cover" />} style={{ width: "auto" }} bodyStyle={{ padding: "8px" }} className="drop-shadow">
              <p className="text-body-14 mb-1 truncate">{item.productName}</p>
              <p className="text-body-10 text-neutral03 mb-1">{item.categoryName}</p>
              <p className="text-body-14 mb-1">
                Rp{" "}
                {parseInt(item.price)
                  .toFixed(2)
                  .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                  .toString()}
              </p>
            </Card>
          </Link>
        )}
      </div>
    );
  });
}

export default CardProduct;
