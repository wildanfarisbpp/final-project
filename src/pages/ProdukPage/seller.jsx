import React, { Component, useState, useEffect } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Navbar from "../../Components/Navbar";

import { Modal, Alert, Empty } from 'antd';
import { Link, useParams } from "react-router-dom";
import LoadingWithBackground from "../../Components/Loading/withBackground";
import { useDispatch, useSelector } from "react-redux";
import { getProductById, deleteProductById } from "../../stores/actions/product";

export class Fade extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const settings = {
      dots: true,
      fade: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      <div className="relative overflow-hidden md:rounded-2xl">
        <Slider ref={(c) => (this.slider = c)} {...settings}>
          <div>
            <img className="w-full" src={ this.props.data.image1 || 'https://via.placeholder.com/400x332?text=No+Image' } alt="produk" />
          </div>
          { this.props.data.image2 &&
          <div>
            <img className="w-full" src={ this.props.data.image2 } alt="produk" />
          </div>
          }
          { this.props.data.image3 &&
          <div>
            <img className="w-full" src={ this.props.data.image3 } alt="produk" />
          </div>
          }
          { this.props.data.image4 &&
          <div>
            <img className="w-full" src={ this.props.data.image4 } alt="produk" />
          </div>
          }
        </Slider>
        <div className="btn-carousel hidden md:block absolute left-3 top-[45%] ">
          <button className=" rounded-full " onClick={this.previous}>
            <img className="rotate-180" src="/images/carousel-btn.png" alt="btn-carousel" />
          </button>
        </div>
        <div className="btn-carousel hidden md:block absolute right-3 top-[45%] ">
          <button className=" rounded-full " onClick={this.next}>
            <img src="/images/carousel-btn.png" alt="btn-carousel" />
          </button>
        </div>
      </div>
    );
  }
}

function ProdukPageSeller() {
  const { id } = useParams()

  const dispatch = useDispatch()
  const { 
    getDetailProductResult, getDetailProductLoading, getDetailProductError,
    getDeleteProductResult, getDeleteProductLoading, getDeleteProductError
  } = useSelector((state) => state.productReducer);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => { setIsModalVisible(true); };
  const handleModalOk = () => { 
    dispatch(deleteProductById({id: id, token: accessToken}))
    setIsModalVisible(false); 
  };
  const handleModalCancel = () => { setIsModalVisible(false); };

   // untuk memunculkan alert
   const [showAlert, setShowAlert] = useState(false);

  // untuk menyimpan token user
  let accessToken
  if (JSON.parse(localStorage.getItem('userLocal')) !== null) {
    accessToken = JSON.parse(localStorage.getItem('userLocal')).accessToken
  } else {
    accessToken = null
  }

  // jika sudah selesai loading & dapat result
  if (getDeleteProductResult) {
    localStorage.setItem('setAlertSuccessProductDelete', true)
    window.location.replace('/daftar-jual/semua')
  } else if (getDeleteProductError) {
    localStorage.setItem('setAlertErrorProductDelete', true)
    window.location.reload()
  }

  if (getDetailProductResult) {
    if (getDetailProductResult.image1 !== null) {
      localStorage.setItem("productImage1", JSON.stringify(getDetailProductResult.image1))
    } else {
      localStorage.removeItem("productImage1")
    }
    if (getDetailProductResult.image2 !== null) {
      localStorage.setItem("productImage2", JSON.stringify(getDetailProductResult.image2))
    } else {
      localStorage.removeItem("productImage2")
    }
    if (getDetailProductResult.image3 !== null) {
      localStorage.setItem("productImage3", JSON.stringify(getDetailProductResult.image3))
    } else {
      localStorage.removeItem("productImage3")
    }
    if (getDetailProductResult.image4 !== null) {
      localStorage.setItem("productImage4", JSON.stringify(getDetailProductResult.image4))
    } else {
      localStorage.removeItem("productImage4")
    }
  }

  useEffect(() => {
    dispatch(getProductById({id: id, token: accessToken})); // untuk get info produk yang sudah ada

    // untuk menampilkan alert setelah tambah produk
    if (JSON.parse(localStorage.getItem('setAlertErrorProductDelete')) === true) {
      setShowAlert("Error")
      localStorage.setItem('setAlertErrorProductDelete', false)
    }

  }, [dispatch, id, accessToken]);

  return (
    <>
      <Navbar />

    { getDeleteProductLoading && <LoadingWithBackground /> }
    { showAlert === "Error" && <Alert message={"Produk gagal dihapus !"} type="error" closable className="custom-alert" /> }

    { getDetailProductLoading 
    ? (<LoadingWithBackground />)
    : ( getDetailProductResult 
      ? <div className="container mx-auto max-w-4xl mt-5 md:py-5">
        <div className="grid md:grid-cols-5 grid-cols-1 gap-6">
          <div className="col-span-3">
            <Fade data={getDetailProductResult} />
            {/* responsive */}
            <div className="container mx-auto max-w-4xl mt-3 -translate-y-12">
              <div className="shadow-auto rounded-lg p-4 bg-white text-base font-medium h-auto mx-4 px-3 md:hidden">
                <h2 className="text-base font-medium break-words">{getDetailProductResult.productName}</h2>
                <p className="text-sm font-normal text-[#8A8A8A]">{getDetailProductResult.category}</p>
                <p className="font-normal text-base">Rp {getDetailProductResult.price}</p>
              </div>
            </div>
            {/* responsive penjual */}
            <div className="container mx-auto max-w-4xl mt-3 -translate-y-11">
              <div className="flex md:hidden shadow-auto rounded-lg bg-white p-4 text-base font-medium  mx-4 px-3">
                <img className="w-12" src={ getDetailProductResult.imageProfil || 'https://via.placeholder.com/400x400?text=No+Image' } alt="user" />
                <div className="ml-4">
                  <h1 className="font-medium text-sm break-words">{getDetailProductResult.fullname}</h1>
                  <h1 className="text-sm font-normal text-[#8A8A8A] ">{getDetailProductResult.city}</h1>
                </div>
              </div>
            </div>

            {/* responsive end */}
            <div className="container mx-auto max-w-4xl mt-5">
              <div className="col-span-3 px-4 md:px-0">
                <div className=" shadow-auto rounded-lg bg-white p-4 -translate-y-11 md:-translate-y-0 ">
                  <h2>Deskripsi</h2>
                  <h1 className="py-2 text-[#8A8A8A] break-all">{getDetailProductResult.description}</h1>
                </div>
              </div>
              <div className="flex flex-row justify-between w-screen gap-x-3 px-4">
                <Link to={`/edit-produk/${id}`} className='w-full'>
                <button className="md:hidden bg-[#7126B5] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white bottom-5">Edit</button>
                </Link>
                <button onClick={showModal} className="md:hidden bg-[#FA2C5A] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white bottom-5">Delete</button>
              </div>
            </div>
          </div>
          <div className="col-span-2">
            <div className="shadow-auto rounded-lg p-5 md:block hidden">
              <h2 className="text-base font-medium break-words">{getDetailProductResult.productName}</h2>
              <p className="text-sm font-normal text-[#8A8A8A]">{getDetailProductResult.category}</p>
              <p className="font-normal text-base">Rp {getDetailProductResult.price}</p>
              <Link to={`/edit-produk/${id}`}>
              <button className="bg-[#7126B5] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white">Edit</button>
              </Link>
              <button onClick={showModal} className="bg-[#FA2C5A] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white">Delete</button>
            </div>
            <div className="col-span-2 mt-5">
              <div className="hidden md:flex shadow-auto rounded-2xl p-4 ">
                <img className="w-12" src={ getDetailProductResult.imageProfil || 'https://via.placeholder.com/400x400?text=No+Image' } alt="user" />
                <div className="ml-4">
                  <h1 className="font-medium text-sm break-words">{getDetailProductResult.fullname}</h1>
                  <h1 className="text-sm font-normal text-[#8A8A8A]">{getDetailProductResult.city}</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      : <div className="w-screen h-screen flex justify-center items-center">
          <Empty/>
        </div>
    ) }
      <Modal title="Confirmation" centered visible={isModalVisible} onOk={handleModalOk} onCancel={handleModalCancel}>
        <span className="text-base">Are you sure you want to <strong>DELETE</strong> this product ?</span>
      </Modal>
    </>
  );
}

export default ProdukPageSeller;
