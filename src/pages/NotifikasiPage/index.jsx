import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Divider, Badge } from "antd";
import Navbar from "../../Components/Navbar";
import { useSelector, useDispatch } from "react-redux";
import { getNotification, editNotification } from "../../stores/actions/notification";
import moment from "moment";
import LoadingWithBackground from "../../Components/Loading/withBackground";

function NotifikasiPage() {
  const [isDesktop, setDesktop] = useState(window.innerWidth >= 640);
  const [isRead, setIsRead] = useState(false);
  const dispatch = useDispatch();
  const token = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  const { getNotificationResult, getNotificationLoading, getEditNotificationResult } = useSelector((state) => state.notificationReducer);

  const updateMedia = () => {
    setDesktop(window.innerWidth >= 640);
  };

  useEffect(() => {
    dispatch(getNotification({ token }));
  }, [dispatch]);

  // useEffect(() => {
  //   const res = dispatch(editNotification({ id, token }));
  //   setIsRead(res);
  // }, [dispatch, id]);

  useEffect(() => {
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  });
  return (
    <>
      <Navbar />
      {getNotificationLoading && <LoadingWithBackground />}
      <div className="flex w-full justify-center px-4 pt-4 pb-6 sm:px-0 sm:py-10">
        <div className="w-full sm:w-4/6 flex flex-col gap-4 sm:gap-6">
          {isDesktop ? (
            <span className="text-xl leading-7 font-bold">Notifikasi</span>
          ) : (
            <div className="flex flex-row items-center gap-4">
              <div className="p-3">
                <img src="/assets/images/fi_menu.svg" alt="Icon menu" className="h-6" />
              </div>
              <span className="text-xl leading-7 font-bold">Notifikasi</span>
            </div>
          )}
          {getNotificationResult &&
            getNotificationResult.map((notifikasi) => {
              return notifikasi.title === "Penawaran produk" ? (
                <Link to={`/info-penawar/${notifikasi.offerId}`}>
                  <div className="grow h-min flex flex-col items-center w-full sm:w-auto pr-3">
                    <div className="bg-neutral01 flex flex-row flex-wrap gap-4 w-full hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                      <img src={notifikasi.image1} alt="produk" className="w-12 h-12 object-cover rounded-xl" />
                      <div className="flex flex-col gap-1 grow">
                        <span className="text-body-10 leading-3 font-normal text-neutral03">{notifikasi.title}</span>
                        <span className="text-sm font-normal text-neutral05 max-w-[160px] md:max-w-none truncate">{notifikasi.productName}</span>
                        <span className="text-sm font-normal text-neutral05">
                          Rp{" "}
                          {parseInt(notifikasi.price)
                            .toFixed(2)
                            .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                            .toString()}
                        </span>
                        <span className="text-sm font-normal text-neutral05">
                          {" "}
                          Ditawar Rp{" "}
                          {parseInt(notifikasi.priceNegotiated)
                            .toFixed(2)
                            .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                            .toString()}
                        </span>
                      </div>
                      <span className="text-body-10 leading-3 font-normal text-neutral03">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                      <span className={notifikasi.isRead === true ? `w-2 h-2 bg-alert-danger rounded-full` : `hidden`}></span>
                    </div>
                  </div>
                </Link>
              ) : (
                notifikasi.title === "Produk berhasil diterbitkan" && (
                  <Link to={`/produk-seller/${notifikasi.productId}`}>
                    <div className="grow h-min flex flex-col items-center w-full sm:w-auto pr-3">
                      <div className="bg-neutral01 flex flex-row flex-wrap gap-4 w-full hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                        <img src={notifikasi.image1} alt="produk" className="w-12 h-12 object-cover rounded-xl" />
                        <div className="flex flex-col gap-1 grow">
                          <span className="text-body-10 leading-3 font-normal text-neutral03">{notifikasi.title}</span>
                          <span className="text-sm font-normal text-neutral05 max-w-[160px] md:max-w-none truncate">{notifikasi.productName}</span>
                          <span className="text-sm font-normal text-neutral05">
                            Rp{" "}
                            {parseInt(notifikasi.price)
                              .toFixed(2)
                              .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                              .toString()}
                          </span>
                        </div>
                        <span className="text-body-10 leading-3 font-normal text-neutral03">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                        <span className={notifikasi.isRead === true ? `w-2 h-2 bg-alert-danger rounded-full` : `hidden`}></span>
                      </div>
                    </div>
                  </Link>
                )
              );
            })}

          {getNotificationResult &&
            getNotificationResult.map((notifikasi) => {
              return notifikasi.info === "penawaran anda telah diterima" ? (
                <Link to={`/info-penawar/${notifikasi.offerId}`}>
                  <div className="grow h-min flex flex-col items-center w-full sm:w-auto pr-3">
                    <div className="bg-neutral01 flex flex-row flex-wrap gap-4 w-full hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                      <img src={notifikasi.image1} alt="produk" className="w-12 h-12 object-cover rounded-xl" />
                      <div className="flex flex-col gap-1 grow">
                        <span className="text-body-10 leading-3 font-normal text-neutral03">{notifikasi.title}</span>
                        <span className="text-sm font-normal text-neutral05 max-w-[160px] md:max-w-none truncate">{notifikasi.productName}</span>
                        <span className="text-sm font-normal text-neutral05">
                          Rp{" "}
                          {parseInt(notifikasi.price)
                            .toFixed(2)
                            .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                            .toString()}
                        </span>
                        <span className="text-sm font-normal text-neutral05">
                          {" "}
                          Ditawar Rp{" "}
                          {parseInt(notifikasi.priceNegotiated)
                            .toFixed(2)
                            .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                            .toString()}
                        </span>
                      </div>
                      <span className="text-body-10 leading-3 font-normal text-neutral03">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                      <span className={notifikasi.isRead === true ? `w-2 h-2 bg-alert-danger rounded-full` : `hidden`}></span>
                    </div>
                  </div>
                </Link>
              ) : (
                notifikasi.info === "penawaran anda telah ditolak" && (
                  <Link to={`/produk-seller/${notifikasi.productId}`}>
                    <div className="grow h-min flex flex-col items-center w-full sm:w-auto pr-3">
                      <div className="bg-neutral01 flex flex-row flex-wrap gap-4 w-full hover:cursor-pointer" onClick={() => dispatch(editNotification({ id: notifikasi.id, token }))}>
                        <img src={notifikasi.image1} alt="produk" className="w-12 h-12 object-cover rounded-xl" />
                        <div className="flex flex-col gap-1 grow">
                          <span className="text-body-10 leading-3 font-normal text-neutral03">{notifikasi.title}</span>
                          <span className="text-sm font-normal text-neutral05 max-w-[160px] md:max-w-none truncate">{notifikasi.productName}</span>
                          <span className="text-sm font-normal text-neutral05">
                            Rp{" "}
                            {parseInt(notifikasi.price)
                              .toFixed(2)
                              .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                              .toString()}
                          </span>
                        </div>
                        <span className="text-body-10 leading-3 font-normal text-neutral03">{moment(notifikasi.createdAt).format("DD MMMM h:mm")}</span>
                        <span className={notifikasi.isRead === true ? `w-2 h-2 bg-alert-danger rounded-full` : `hidden`}></span>
                      </div>
                    </div>
                  </Link>
                )
              );
            })}
        </div>
      </div>
    </>
  );
}

export default NotifikasiPage;
