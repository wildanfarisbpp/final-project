import React, { Component, useState, useEffect } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { AiOutlineClose } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import { addOffer } from "../../stores/actions/offer";
import { useMediaQuery } from "react-responsive";
import Navbar from "../../Components/Navbar";

import { Alert, Empty } from "antd";
import { useParams, Link } from "react-router-dom";
import LoadingWithBackground from "../../Components/Loading/withBackground";
import { getProductById } from "../../stores/actions/product";

export class Fade extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const settings = {
      dots: true,
      fade: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      <div className="relative overflow-hidden md:rounded-2xl">
        <Slider ref={(c) => (this.slider = c)} {...settings}>
          <div>
            <img className="w-full" src={this.props.data.image1 || "https://via.placeholder.com/400x332?text=No+Image"} alt="produk" />
          </div>
          {this.props.data.image2 && (
            <div>
              <img className="w-full" src={this.props.data.image2} alt="produk" />
            </div>
          )}
          {this.props.data.image3 && (
            <div>
              <img className="w-full" src={this.props.data.image3} alt="produk" />
            </div>
          )}
          {this.props.data.image4 && (
            <div>
              <img className="w-full" src={this.props.data.image4} alt="produk" />
            </div>
          )}
        </Slider>
        <div className="btn-carousel hidden md:block absolute left-3 top-[45%]">
          <button className=" rounded-full" onClick={this.previous}>
            <img className="rotate-180" src="/images/carousel-btn.png" alt="btn-carousel" />
          </button>
        </div>
        <div className="btn-carousel hidden md:block absolute right-3 top-[45%] ">
          <button className=" rounded-full " onClick={this.next}>
            <img src="/images/carousel-btn.png" alt="btn-carousel" />
          </button>
        </div>
      </div>
    );
  }
}

function ProdukPageBuyer() {
  const { id } = useParams();
  const { getDetailProductResult, getDetailProductLoading } = useSelector((state) => state.productReducer);

  const [isOpen, setIsOpen] = useState(false);
  const [alertOpen, setAlertOpen] = useState(true);
  const [buttonClick, setButtonClick] = useState(true);
  const [offerPrice, setOfferPrice] = useState({ price: "" });
  const dispatch = useDispatch();
  console.log(offerPrice);
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1024px)",
  });

  // untuk menyimpan token user
  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(addOffer(id, offerPrice, accessToken));
    console.log(offerPrice);
  };

  useEffect(() => {
    dispatch(getProductById({ id: id, token: accessToken })); // untuk get info produk yang sudah ada
  }, [dispatch, id, accessToken]);

  return (
    <>
      {isDesktopOrLaptop && <Navbar />}
      {!isDesktopOrLaptop && (
        <Link to="/">
          <img src="/assets/images/fi_arrow-left-bg-white.svg" className="absolute z-50 mx-4 my-11 cursor-pointer" alt="<" />
        </Link>
      )}
      {!accessToken && (
        <>
          <div className="hidden">
            {window.setTimeout(() => {
              window.location.href = "/login";
            }, 3000)}
          </div>
          <Alert message={"Silahkan Login atau lengkapi profile terlebih dahulu !!"} />
        </>
      )}
      {getDetailProductLoading ? (
        <LoadingWithBackground />
      ) : (
        getDetailProductResult && (
          <section className="container mx-auto max-w-4xl md:py-5">
            <div className="grid md:grid-cols-5 grid-cols-1 gap-6">
              <div className="col-span-3 ">
                <Fade data={getDetailProductResult} />
                {/* responsive */}
                <div className="container mx-auto max-w-4xl mt-3 -translate-y-12">
                  <div className="shadow-auto rounded-lg p-4 bg-white text-base font-medium h-28 mx-4 px-3 md:hidden">
                    <h2 className="text-base font-medium">{getDetailProductResult.productName}</h2>
                    <p className="text-sm font-normal text-[#8A8A8A]">{getDetailProductResult.category}</p>
                    <p className="font-normal text-base">
                      Rp{" "}
                      {parseInt(getDetailProductResult.price)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                        .toString()}
                    </p>
                  </div>
                </div>
                {/* responsive penjual */}
                <div className="container mx-auto max-w-4xl mt-3 -translate-y-11">
                  <div className="flex md:hidden shadow-auto rounded-lg bg-white p-4 text-base font-medium  mx-4 px-3">
                    <img className="w-12" src={getDetailProductResult.imageProfil} alt="profil seller" />
                    <div className="ml-4">
                      <h1 className="font-medium text-sm">{getDetailProductResult.fullname}</h1>
                      <h1 className="text-sm font-normal text-[#8A8A8A]">{getDetailProductResult.city}</h1>
                    </div>
                  </div>
                </div>
                <div className="container mx-auto max-w-4xl mt-5">
                  <div className="col-span-3 px-4 md:px-0">
                    <div className="shadow-auto rounded-lg p-4 -translate-y-11 md:-translate-y-0 ">
                      <h2>Deskripsi</h2>
                      <h1 className="py-2 text-[#8A8A8A] break-all">{getDetailProductResult.description}</h1>
                    </div>
                  </div>
                </div>
                {getDetailProductResult.username === JSON.parse(localStorage.getItem("userLocal")).username ? (
                  <div className="flex justify-center">
                    <button disabled className="md:hidden bg-gray-300 cursor-not-allowed w-11/12 py-4 mt-2 rounded-2xl font-medium text-sm text-white fixed bottom-5" id="tertarik-btn">
                      Saya tertarik dan ingin nego
                    </button>
                  </div>
                ) : (
                  <div className="flex justify-center">
                    {buttonClick ? (
                      <button onClick={() => setIsOpen((current) => !current)} className="md:hidden bg-[#7126B5] w-11/12 py-4 mt-2 rounded-2xl font-medium text-sm text-white fixed bottom-5" id="tertarik-btn">
                        Saya tertarik dan ingin nego
                      </button>
                    ) : (
                      <button className="md:hidden bg-[#D0D0D0] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white fixed bottom-5" id="tertarik-btn">
                        Menunggu respon penjual
                      </button>
                    )}
                  </div>
                )}
              </div>
              <div className="col-span-2">
                <div className="shadow-auto rounded-lg p-5 md:block hidden">
                  <h2 className="text-base font-medium">{getDetailProductResult.productName}</h2>
                  <p className="text-sm font-normal text-[#8A8A8A]">{getDetailProductResult.category}</p>
                  <p className="font-normal text-base">
                    Rp{" "}
                    {parseInt(getDetailProductResult.price)
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                      .toString()}
                  </p>
                  {/* modals */}
                  {getDetailProductResult.username === JSON.parse(localStorage.getItem("userLocal")).username ? (
                    <button disabled className="bg-gray-300 cursor-not-allowed w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white" id="tertarik-btn">
                      Saya tertarik dan ingin nego
                    </button>
                  ) : (
                    <>
                      {buttonClick ? (
                        <button onClick={() => setIsOpen(true)} className="bg-[#7126B5] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white" id="tertarik-btn">
                          Saya tertarik dan ingin nego
                        </button>
                      ) : (
                        <button className="bg-[#D0D0D0] w-full py-3 mt-2 rounded-2xl font-medium text-sm text-white" id="tertarik-btn">
                          Menunggu respon penjual
                        </button>
                      )}
                    </>
                  )}
                  {/* alert */}
                </div>
                <div className={`bg-[#73CA5C] rounded-xl fixed top-5 w-80 md:w-[500px] inset-x-1/2 -translate-x-1/2 px-6 py-2 justify-between flex text-white ${alertOpen ? "hidden" : ""}`}>
                  <h2 className="text-white my-auto">Harga tawarmu berhasil dikirim ke penjual</h2>
                  <div role="button" onClick={() => setAlertOpen(true)} className="my-auto">
                    <AiOutlineClose />
                  </div>
                </div>
                <div className={`bg-black bg-opacity-50 fixed top-0 left-0 inset-0 ${isOpen ? "flex" : "hidden"} justify-center items-center `} id="overlay"></div>
                <div
                  className={`bg-white rounded-t-2xl md:rounded-2xl w-full md:w-auto fixed transition ease-in-out duration-700 md:transition-none ${
                    isOpen ? "left-1/2 -translate-x-1/2 h-full md:h-auto top-1/4 md:-translate-y-1/2 md:top-1/2" : "translate-y-full -bottom-12 left-1/2 -translate-x-1/2 md:scale-0"
                  }`}
                >
                  <div className="flex justify-center md:justify-end items-center pr-4 pt-3">
                    <button onClick={() => setIsOpen(false)} className="hidden md:block">
                      <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor" id="close-modal">
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </button>
                    <button onClick={() => setIsOpen(false)}>
                      <img className="md:hidden" src="/images/button-mobile.png" alt="button mobile" />
                    </button>
                  </div>
                  <div>
                    <h4 className="text-base font-medium ml-2 pt-5 px-6">Masukan Harga Tawarmu</h4>
                  </div>
                  <div className="w-[350px] px-6">
                    <p className="ml-2 pt-2 text-gray-500 ">Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan segera dihubungi penjual.</p>
                  </div>
                  <div className="col-span-2 pt-1 p-6">
                    <div className="flex shadow-auto rounded-2xl p-3 bg-[#b8b7b776]">
                      <img className="w-14 rounded-xl object-cover" src={getDetailProductResult.image1 || "https://via.placeholder.com/400x400?text=No+Image"} alt="foto produk 1" />
                      <div className="ml-4">
                        <h1 className="font-medium text-sm">{getDetailProductResult.productName}</h1>
                        <h1 className="text-sm font-normal ">
                          Rp{" "}
                          {parseInt(getDetailProductResult.price)
                            .toFixed(2)
                            .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                            .toString()}
                        </h1>
                      </div>
                    </div>
                    <div className="pt-5 ml-2">
                      <h1>Harga Tawar</h1>
                    </div>
                    <div>
                      <div>
                        <div className="flex py-4 bg-[#fffcfc]">
                          <form onSubmit={(event) => handleSubmit(event)}>
                            <input type="number" name="offerPrice" placeholder="Rp 0,00" className="w-full rounded-xl focus:outline-none" onChange={(event) => setOfferPrice({ ...offerPrice, price: event.target.value.toString() })} />
                            <input disabled />
                            <button
                              type="submit"
                              onClick={() => {
                                setAlertOpen(false);
                                setIsOpen(false);
                                setButtonClick(false);
                              }}
                              className="bg-[#7126B5] w-full py-3 mt-5 rounded-2xl font-medium text-sm text-white"
                              id="tertarik-btn"
                            >
                              Kirim
                            </button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* modals end */}

                <div className="col-span-2 mt-5">
                  <div className=" md:flex hidden shadow-auto rounded-2xl p-4">
                    <img className="w-12" src={getDetailProductResult.imageProfil} alt="profile seller" />
                    <div className="ml-4">
                      <h1 className="font-medium text-sm">{getDetailProductResult.fullname}</h1>
                      <h1 className="text-sm font-normal text-[#8A8A8A]">{getDetailProductResult.city}</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )
      )}
    </>
  );
}

export default ProdukPageBuyer;
