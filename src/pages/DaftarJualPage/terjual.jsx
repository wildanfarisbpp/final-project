import { Card, Skeleton } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { productSold } from "../../stores/actions/history";

function ProdukTerjual() {
  const dispatch = useDispatch();
  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  const { getProductSoldResult, getProductSoldLoading } = useSelector((state) => state.historyReducer);

  useEffect(() => {
    dispatch(productSold({ token: accessToken }));
  }, [accessToken, dispatch]);

  return (
    <>
      {getProductSoldLoading && (
        <div className="flex flex-wrap">
          <Skeleton active loading={true} style={{ width: "300px" }} />
        </div>
      )}

      {getProductSoldResult.length === 0 && (
        <div className="grow h-min flex w-full pb-6">
          <div className="max-w-[280px] flex flex-col mx-auto justify-center items-center gap-6">
            <img src="/assets/images/Group 33.svg" alt="No Product" />
            <span className="text-sm font-normal text-center">Belum ada produkmu yang terjual nih, sabar ya rejeki nggak kemana kok</span>
          </div>
        </div>
      )}
      <div className="grow h-min gap-6 grid grid-cols-2 lg:grid-cols-3">
        {getProductSoldResult &&
          getProductSoldResult.map((product) => {
            return (
              <Card cover={<img src={product.image1} alt="produk" className="px-2 pt-2 h-[100px] object-cover" />} style={{ width: "auto" }} bodyStyle={{ padding: "8px" }} className="drop-shadow">
                <p className="text-body-14 mb-1 truncate">{product.productName}</p>
                <p className="text-body-10 text-neutral03 mb-1">{product.categoryName}</p>
                <p className="text-body-14 mb-1">
                  Rp{" "}
                  {parseInt(product.price)
                    .toFixed(2)
                    .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                    .toString()}
                </p>
              </Card>
            );
          })}
      </div>
    </>
  );
}

export default ProdukTerjual;
