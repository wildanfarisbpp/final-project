import React, { useState, useEffect } from "react";
import { Link, Outlet, useLocation } from "react-router-dom";
import { Alert } from "antd";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import Navbar from "../../Components/Navbar";

import LoadingWithBackground from "../../Components/Loading/withBackground";
import { useDispatch, useSelector } from "react-redux";
import { getUserProfile } from "../../stores/actions/profile";

function DaftarJualPage() {
  const location = useLocation();

  const dispatch = useDispatch();
  const { getDetailProfileResult, getDetailProfileLoading } = useSelector((state) => state.profileReducer);

  // function mengupdate state ukuran layar apakah desktop atau mobile
  const [isDesktop, setDesktop] = useState(window.innerWidth >= 640);
  const updateMedia = () => {
    setDesktop(window.innerWidth >= 640);
  };

  // untuk memunculkan alert
  const [showAlert, setShowAlert] = useState(false);

  // untuk menyimpan token user
  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  if (getDetailProfileResult.imageProfile) {
    const initialImages = {
      uid: "profile_image",
      name: "profile_image.png",
      status: "done",
      url: getDetailProfileResult.imageProfile,
    };
    localStorage.setItem("userImage", JSON.stringify(initialImages));
  }

  useEffect(() => {
    // untuk menampilkan alert berhasil disimpan setelah tambah produk berhasil
    if (JSON.parse(localStorage.getItem("setAlertSuccessProfile")) === true) {
      setShowAlert("Berhasil-Profile");
      localStorage.setItem("setAlertSuccessProfile", false);
    } else if (JSON.parse(localStorage.getItem("setAlertSuccessProduct")) === true) {
      setShowAlert("Berhasil-Product");
      localStorage.setItem("setAlertSuccessProduct", false);
    } else if (JSON.parse(localStorage.getItem("setAlertSuccessProductDelete")) === true) {
      setShowAlert("Berhasil-Product-Delete");
      localStorage.setItem("setAlertSuccessProductDelete", false);
    }

    dispatch(getUserProfile(accessToken)); // mengambil detail user profile

    // untuk mengupdate state ukuran layar apakah desktop atau mobile
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  }, [dispatch, accessToken]);

  return (
    <>
      {showAlert === "Berhasil-Profile" && <Alert message={"Edit profil berhasil disimpan !"} type="success" closable className="custom-alert" />}
      {showAlert === "Berhasil-Product" && <Alert message={"Produk berhasil diterbitkan !"} type="success" closable className="custom-alert" />}
      {showAlert === "Berhasil-Product-Delete" && <Alert message={"Produk berhasil dihapus !"} type="success" closable className="custom-alert" />}
      <Navbar />
      {getDetailProfileLoading ? (
        <LoadingWithBackground />
      ) : (
        getDetailProfileResult && (
          <div className="flex w-full justify-center p-4 sm:px-0 sm:py-10">
            <div className="w-full sm:max-w-lg md:max-w-3xl flex flex-col gap-6">
              {isDesktop ? (
                <span className="text-xl leading-7 font-bold">Daftar Jual Saya</span>
              ) : (
                <div className="flex flex-row items-center gap-4">
                  <div className="p-3">
                    <img src="/assets/images/fi_menu.svg" alt="Icon menu" className="h-6" />
                  </div>
                  <span className="text-xl leading-7 font-bold">Daftar Jual Saya</span>
                </div>
              )}
              <div className="p-4 bg-neutral01 rounded-2xl flex flex-row flex-wrap gap-4 items-center w-full" style={{ boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.15)" }}>
                <img src={getDetailProfileResult.imageProfile || "https://via.placeholder.com/400x400?text=No+Image"} alt="Profile User" className="w-12 h-12 object-cover rounded-xl" />
                <div className="flex flex-col gap-1 grow">
                  <span className="text-sm font-medium">{getDetailProfileResult.fullname}</span>
                  <span className="text-body-10 leading-3 text-neutral03">{getDetailProfileResult.city}</span>
                </div>
                <Link to="/info-profil">
                  <button className="px-3 py-1 bg-neutral01 border rounded-lg border-primary-darkblue04 font-medium text-xs text-neutral05">Edit</button>
                </Link>
              </div>
              <div className="flex flex-col sm:flex-row justify-center items-center sm:items-start gap-8 w-full">
                {isDesktop ? (
                  <div className="p-6 bg-neutral01 rounded-2xl flex flex-col gap-6 h-min min-w-[200px]" style={{ boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)" }}>
                    <span className="text-base font-medium">Kategori</span>
                    <div className="flex flex-col gap-4">
                      <Link to="/daftar-jual/semua">
                        {location.pathname === "/daftar-jual/semua" ? (
                          <div className="pb-4 border-b border-neutral02 w-full flex flex-row justify-between">
                            <img src="/assets/images/fi_box-active.svg" alt="Icon box" className="mr-3" />
                            <span className="text-base font-medium text-primary-darkblue04 grow mr-4">Semua Produk</span>
                            <img src="/assets/images/fi_chevron-right-active.svg" alt="Icon chevron right" />
                          </div>
                        ) : (
                          <div className="pb-4 border-b border-neutral02 w-full flex flex-row justify-between">
                            <img src="/assets/images/fi_box.svg" alt="Icon box" className="mr-3" />
                            <span className="text-base font-normal text-neutral05 grow mr-4">Semua Produk</span>
                            <img src="/assets/images/fi_chevron-right.svg" alt="Icon chevron right" />
                          </div>
                        )}
                      </Link>
                      <Link to="/daftar-jual/diminati">
                        {location.pathname === "/daftar-jual/diminati" ? (
                          <div className="pb-4 border-b border-neutral02 w-full flex flex-row justify-between">
                            <img src="/assets/images/fi_heart-active.svg" alt="Icon box" className="mr-3" />
                            <span className="text-base font-medium text-primary-darkblue04 grow mr-4">Diminati</span>
                            <img src="/assets/images/fi_chevron-right-active.svg" alt="Icon chevron right" />
                          </div>
                        ) : (
                          <div className="pb-4 border-b border-neutral02 w-full flex flex-row justify-between">
                            <img src="/assets/images/fi_heart.svg" alt="Icon box" className="mr-3" />
                            <span className="text-base font-normal text-neutral05 grow mr-4">Diminati</span>
                            <img src="/assets/images/fi_chevron-right.svg" alt="Icon chevron right" />
                          </div>
                        )}
                      </Link>
                      <Link to="/daftar-jual/terjual">
                        {location.pathname === "/daftar-jual/terjual" ? (
                          <div className="pb-4 w-full flex flex-row justify-between">
                            <img src="/assets/images/fi_dollar-sign-active.svg" alt="Icon box" className="mr-3" />
                            <span className="text-base font-medium text-primary-darkblue04 grow mr-4">Terjual</span>
                            <img src="/assets/images/fi_chevron-right-active.svg" alt="Icon chevron right" />
                          </div>
                        ) : (
                          <div className="pb-4 w-full flex flex-row justify-between">
                            <img src="/assets/images/fi_dollar-sign.svg" alt="Icon box" className="mr-3" />
                            <span className="text-base font-normal text-neutral05 grow mr-4">Terjual</span>
                            <img src="/assets/images/fi_chevron-right.svg" alt="Icon chevron right" />
                          </div>
                        )}
                      </Link>
                    </div>
                  </div>
                ) : (
                  <Swiper slidesPerView={"auto"} spaceBetween={16} className="mySwiper w-full h-full">
                    <SwiperSlide style={{ width: "auto", display: "block" }}>
                      <Link to="/daftar-jual/semua">
                        {location.pathname === "/daftar-jual/semua" ? (
                          <button className="flex items-center gap-2 bg-primary-darkblue04 rounded-xl py-3 px-4 font-normal text-sm text-neutral01">
                            <img src="/assets/images/fi_box-white.svg" alt="Icon box" />
                            Produk
                          </button>
                        ) : (
                          <button className="flex items-center gap-2 bg-primary-darkblue01 rounded-xl py-3 px-4 font-normal text-sm text-neutral04">
                            <img src="/assets/images/fi_box-black.svg" alt="Icon box" />
                            Produk
                          </button>
                        )}
                      </Link>
                    </SwiperSlide>
                    <SwiperSlide style={{ width: "auto", display: "block" }}>
                      <Link to="/daftar-jual/diminati">
                        {location.pathname === "/daftar-jual/diminati" ? (
                          <button className="flex items-center gap-2 bg-primary-darkblue04 rounded-xl py-3 px-4 font-normal text-sm text-neutral01">
                            <img src="/assets/images/fi_heart-white.svg" alt="Icon box" />
                            Diminati
                          </button>
                        ) : (
                          <button className="flex items-center gap-2 bg-primary-darkblue01 rounded-xl py-3 px-4 font-normal text-sm text-neutral04">
                            <img src="/assets/images/fi_heart-black.svg" alt="Icon box" />
                            Diminati
                          </button>
                        )}
                      </Link>
                    </SwiperSlide>
                    <SwiperSlide style={{ width: "auto", display: "block" }}>
                      <Link to="/daftar-jual/terjual">
                        {location.pathname === "/daftar-jual/terjual" ? (
                          <button className="flex items-center gap-2 bg-primary-darkblue04 rounded-xl py-3 px-4 font-normal text-sm text-neutral01">
                            <img src="/assets/images/fi_dollar-sign-white.svg" alt="Icon box" />
                            Terjual
                          </button>
                        ) : (
                          <button className="flex items-center gap-2 bg-primary-darkblue01 rounded-xl py-3 px-4 font-normal text-sm text-neutral04">
                            <img src="/assets/images/fi_dollar-sign-black.svg" alt="Icon box" />
                            Terjual
                          </button>
                        )}
                      </Link>
                    </SwiperSlide>
                  </Swiper>
                )}
                <Outlet />
              </div>
            </div>
          </div>
        )
      )}
    </>
  );
}

export default DaftarJualPage;
