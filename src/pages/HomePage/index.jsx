import React, { useEffect } from "react";
import { Button, Carousel, Empty, Skeleton } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import CardProduct from "../../Components/Card/Card";
import Navbar from "../../Components/Navbar";
import { Link } from "react-router-dom";
import { getAllProduct, getCategory } from "../../stores/actions/product";
import { useDispatch, useSelector } from "react-redux";

function HomePage() {
  const dispatch = useDispatch();
  const queryParams = new URLSearchParams(window.location.search);
  let search = queryParams.get("searchProduct");
  let current_page = queryParams.get("page");
  if (current_page === null) {
    current_page = 1;
  }
  let id_category = queryParams.get("category");
  if (id_category === null) {
    id_category = "semua";
  }

  let accessToken;
  if (JSON.parse(localStorage.getItem("userLocal")) !== null) {
    accessToken = JSON.parse(localStorage.getItem("userLocal")).accessToken;
  } else {
    accessToken = null;
  }

  const { getAllProductResult, getAllProductLoading, getCategoryResult, getAllProductCurrentPage, getAllProductTotalPages } = useSelector((state) => state.productReducer);

  useEffect(() => {
    dispatch(getAllProduct({ search: search, category: id_category, page: current_page }));
    dispatch(getCategory());
  }, [dispatch, search, id_category, current_page]);

  const contentStyle = {
    textAlign: "center",
    margin: "20px",
    width: "100%",
    padding: "20px 60px",
  };

  let pagination = [];
  for (let i = 1; i <= getAllProductTotalPages; i++) {
    if (id_category === "semua") {
      if (i === getAllProductCurrentPage) {
        pagination.push(
          <a href={`?page=${i}`} key={i}>
            <button className="flex items-center mr-3 rounded-lg py-2 px-3 bg-primary-darkblue04 text-neutral01 font-bold">{i}</button>
          </a>
        );
      } else {
        pagination.push(
          <a href={`?page=${i}`} key={i}>
            <button className="flex items-center mr-3 rounded-lg py-2 px-3 bg-primary-darkblue01 text-[#000000] hover:bg-primary-darkblue02 font-bold">{i}</button>
          </a>
        );
      }
    } else {
      if (i === getAllProductCurrentPage) {
        pagination.push(
          <a href={`?category=${id_category}&page=${i}`} key={i}>
            <button className="flex items-center mr-3 rounded-lg py-2 px-3 bg-primary-darkblue04 text-neutral01 font-bold">{i}</button>
          </a>
        );
      } else {
        pagination.push(
          <a href={`?category=${id_category}&page=${i}`} key={i}>
            <button className="flex items-center mr-3 rounded-lg py-2 px-3 bg-primary-darkblue01 text-[#000000] hover:bg-primary-darkblue02 font-bold">{i}</button>
          </a>
        );
      }
    }
  }

  return (
    <div className="pb-8">
      <Navbar />

      {/* banner desktop */}
      <div className="my-5 hidden md:block">
        <Carousel autoplay={true} draggable={true} dots={false} slidesToShow={1} focusOnSelect={true} initialSlide={1} centerMode={true} centerPadding={"130px"}>
          <div className="className=" rounded-xl>
            <img src="/assets/images/banner-1.png" alt="banner" style={contentStyle} className="rounded-xl" />
          </div>
          <div>
            <img src="/assets/images/banner-2.png" alt="banner" style={contentStyle} className="rounded-xl" />
          </div>
        </Carousel>
      </div>

      {/* banner mobile */}
      <div className="block md:hidden">
        <Carousel autoplay={true} draggable={true} dots={false} slidesToShow={1}>
          <div>
            <div
              className="!flex items-center justify-between px-4 sm:px-16"
              style={{
                background: "linear-gradient(180deg, #FFE9C9 59.55%, rgba(255, 233, 202, 0) 100%)",
                height: "398px",
              }}
            >
              <div>
                <p className="text-title-20 font-bold mb-4.5">Bulan Ramadhan Banyak diskon!</p>
                <p className="text-body-10">Diskon Hingga</p>
                <p className="text-[18px] text-alert-danger font-medium">60%</p>
              </div>

              <img src="/assets/images/gift.svg" alt="gift" />
            </div>
          </div>
          <div>
            <div
              className="!flex items-center justify-between px-4 sm:px-16"
              style={{
                background: "linear-gradient(180deg, #FFE9C9 59.55%, rgba(255, 233, 202, 0) 100%)",
                height: "398px",
              }}
            >
              <div>
                <p className="text-title-20 font-bold mb-4.5">Bulan Ramadhan Banyak diskon!</p>
                <p className="text-body-10">Diskon Hingga</p>
                <p className="text-[18px] text-alert-danger font-medium">60%</p>
              </div>

              <img src="/assets/images/gift.svg" alt="gift" />
            </div>
          </div>
        </Carousel>
      </div>
      {/* </div> */}

      {/* content */}
      <div className="container mx-auto my-5 px-4 relative bottom-28 md:bottom-0 w-full md:w-11/12">
        <p className="text-body-14 lg:text-[16px] text-left font-medium lg:font-bold">Telusuri Kategori</p>
        <div className="flex flex-row items-center overflow-x-scroll lg:overflow-x-auto mb-10">
          {id_category === "semua" ? (
            <a href="/">
              <button className="flex items-center bg-primary-darkblue04 text-neutral01 mr-4 lg:mr-5 rounded-lg py-2 px-3">
                <SearchOutlined className="mr-2 text-title-20" />
                Semua
              </button>
            </a>
          ) : (
            <a href="/">
              <button className="flex items-center bg-primary-darkblue01 text-[#000000] hover:bg-primary-darkblue02 mr-4 lg:mr-5 rounded-lg py-2 px-3">
                <SearchOutlined className="mr-2 text-title-20" />
                Semua
              </button>
            </a>
          )}
          {getCategoryResult &&
            getCategoryResult.map((category, i) => {
              return (
                <div key={i}>
                  {id_category === `${category.id}` ? (
                    <a href={`?category=${category.id}`} key={i}>
                      <button className="flex items-center bg-primary-darkblue04 text-neutral01 mr-4 lg:mr-5 rounded-lg py-2 px-3">
                        <SearchOutlined className="mr-2 text-title-20" />
                        {category.name}
                      </button>
                    </a>
                  ) : (
                    <a href={`?category=${category.id}`} key={i}>
                      <button className="flex items-center bg-primary-darkblue01 text-[#000000] hover:bg-primary-darkblue02 mr-4 lg:mr-5 rounded-lg py-2 px-3">
                        <SearchOutlined className="mr-2 text-title-20" />
                        {category.name}
                      </button>
                    </a>
                  )}
                </div>
              );
            })}
        </div>

        <div className="mb-3 grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6 gap-4">
          {getAllProductLoading ? (
            <>
              <div class="flex flex-grow-0">
                <Skeleton active loading={true} />
              </div>
              <div class="flex flex-grow-1">
                <Skeleton active loading={true} />
              </div>
              <div class="flex flex-grow-2">
                <Skeleton active loading={true} />
              </div>
              <div class="flex flex-grow-3">
                <Skeleton active loading={true} />
              </div>
              <div class="flex flex-grow-4">
                <Skeleton active loading={true} />
              </div>
              <div class="flex flex-grow-5">
                <Skeleton active loading={true} />
              </div>
            </>
          ) : (
            getAllProductResult && <CardProduct product={getAllProductResult} />
          )}
        </div>

        {getAllProductResult.length === 0 && <Empty />}

        <div className="flex flex-row justify-end">{pagination}</div>

        {accessToken ? (
          <div className="fixed bottom-5 -translate-x-1/2 left-1/2">
            <Link to={"/info-produk"}>
              <Button className="!bg-primary-darkblue04 !text-neutral01 !py-4 !px-6 !flex items-center !rounded-xl !font-medium !border-none" style={{ boxShadow: "0px 20px 19px -12px #7126b5" }}>
                + Jual
              </Button>
            </Link>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}

export default HomePage;
