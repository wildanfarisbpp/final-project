/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      boxShadow: {
        "auto": "0px 0px 10px 0px #00000026",
      },
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      fontSize: {
        "body-10": "10px",
        "body-12": "12px",
        "body-14": "14px",
        "title-20": "20px",
        "title-24": "24px",
      },
      colors: {
        primary: {
          darkblue01: "#E2D4F0",
          darkblue02: "#D0B7E6",
          darkblue03: "#A06ECE",
          darkblue04: "#7126B5",
          darkblue05: "#4B1979",
          limegreen01: "#FFF8ED",
          limegreen02: "#FFF0DC",
          limegreen03: "#FFE9CA",
          limegreen04: "#D4C2A8",
          limegreen05: "#AA9B87",
        },
        alert: {
          success: "#73CA5C",
          warning: "#F9CC00",
          danger: "#FA2C5A",
        },
        neutral01: "#FFFFFF",
        neutral02: "#D0D0D0",
        neutral03: "#8A8A8A",
        neutral04: "#3C3C3C",
        neutral05: "#151515",
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],

};
